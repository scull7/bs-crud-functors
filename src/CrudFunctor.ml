module type Config = CrudFunctor_model.Config

module Exn = CrudFunctor_exn

module Sql = SqlComposer

module Make(Driver: CrudFunctor_driver.Driver) = struct

  module Id = Driver.Id

  module Response = Driver.Response

  module Params = Driver.Params

  module Model = CrudFunctor_model.Factory(Driver)

  module Query = CrudFunctor_query.Make(Driver)

  module Raw = CrudFunctor_raw.Make(Driver)
end
