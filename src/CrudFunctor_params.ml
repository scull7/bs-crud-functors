(*
 * ## Params
 *)

module Make(Driver: CrudFunctor_driver.Driver) = struct
  open Json

  let positional array =
    array |. Encode.jsonArray |. Driver.Params.positional |. Some


  let encode_id id =
    [| Driver.Id.toJson id |]
    |. Encode.jsonArray
    |. Driver.Params.positional
    |. Some
end
