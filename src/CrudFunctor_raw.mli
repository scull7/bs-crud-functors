
module Make(Driver: CrudFunctor_driver.Driver) : sig

  val query :
    string ->
    string ->
    Driver.Params.t option ->
    Driver.Connection.t ->
    (Driver.Response.Select.t, exn) Belt.Result.t Future.t

  val query_one :
    string ->
    (Js.Json.t -> 'a) ->
    string ->
    Driver.Params.t option ->
    Driver.Connection.t ->
    ('a option, exn) Belt.Result.t Future.t

  val query_many :
    string ->
    (Js.Json.t -> 'a) ->
    string ->
    Driver.Params.t option ->
    Driver.Connection.t ->
    ('a array, exn) Belt.Result.t Future.t

  val mutate :
    string ->
    string ->
    Driver.Params.t option ->
    Driver.Connection.t ->
    (Driver.Response.Mutation.t, exn) Belt.Result.t Future.t

  val mutate_batch :
    string ->
    (string -> exn) ->
    string ->
    ('a -> Js.Json.t array) ->
    string array ->
    'a array ->
    Driver.Connection.t ->
    (int, exn) Belt.Result.t Future.t
end
