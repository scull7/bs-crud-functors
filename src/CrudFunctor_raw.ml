(*
 * ## Raw
 * Direct interface to the driver
 *)
module Exn = CrudFunctor_exn

module DebugConfig = struct
  let name = "CrudFunctor_raw"
end

module Make(Driver: CrudFunctor_driver.Driver) = struct

  module Debug = CrudFunctor_debug.Make(DebugConfig)
  module Decode = CrudFunctor_decode.Make(Driver)

  let mutate name sql params db =
    let _ = Debug.log name sql params
    in
    Driver.Promise.mutate ~db ~sql ?params
    |. FutureJs.fromPromise Exn.mutation

  let mutate_batch name error table encoder columns rows db =
    Driver.Promise.Batch.mutate
      ~db
      ?batch_size:None
      ~table
      ~columns
      ~encoder
      ~rows
      ()
    |. FutureJs.fromPromise (fun e ->
        Js.String.make e
        |. (fun str -> {j|ERROR: $name - $str|j})
        |. error
    )

  let query name sql params db =
    let _ = Debug.log name sql params
    in
    Driver.Promise.query ~db ~sql ?params
    |. FutureJs.fromPromise Exn.select

  let query_one name decoder sql params db =
    query name sql params db
    |. Future.flatMapOk (fun x -> x |. Decode.one_row decoder |. Future.value)

  let query_many name decoder sql params db =
    query name sql params db
    |. Future.flatMapOk (fun x -> x |. Decode.rows decoder |. Future.value)
end
