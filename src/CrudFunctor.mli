
module type Config = CrudFunctor_model.Config

module Exn = CrudFunctor_exn

module Sql = SqlComposer

module Make(Driver: CrudFunctor_driver.Driver) : sig

  module Id : sig
    type t = Driver.Id.t

    val fromJson : Js.Json.t -> t

    val toJson : t -> Js.Json.t

    val toString : t -> string
  end


  module Params : sig
    type t = Driver.Params.t

    val named : Js.Json.t -> t

    val positional : Js.Json.t -> t
  end


  module Response : sig
    module Mutation : sig
      type t

      val insertId : t -> Id.t option

      val affectedRows : t -> int
    end

    module Select : sig
      type t

      val rows : t -> Js.Json.t array
    end
  end


  module Model : sig
    module Make(Config: Config) : sig

      module Read : sig
        val one_by_id :
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t


        val one_by :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t


        val raw :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t array, exn) Belt.Result.t Future.t


        val where :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t array, exn) Belt.Result.t Future.t
      end


      module Create : sig

        val one :
          ('a -> Js.Json.t) ->
          'a ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t


        val batch :
          name:string ->
          encoder:('a -> Js.Json.t array) ->
          loader:('a array -> ('b array, exn) Belt.Result.t Future.t) ->
          error:(string -> exn) ->
          columns:string array ->
          rows:'a array ->
          Driver.Connection.t ->
          ('b array, exn) Belt.Result.t Future.t
      end


      module Update : sig

        val by_id :
          ('a -> Js.Json.t) ->
          'a ->
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t


        val increment_one_by_id :
          string ->
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t
      end


      module Deactivate : sig

        val one_by_id :
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t

      end


      module Archive : sig

        val one_by_id :
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t


        val by :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t array, exn) Belt.Result.t Future.t


        val compound_by :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t array, exn) Belt.Result.t Future.t

        
        val compound_one_by_id :
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t option, exn) Belt.Result.t Future.t

      end


      module Delete : sig
        
        val one_by_id :
          Driver.Id.t ->
          Driver.Connection.t ->
          (Config.t, exn) Belt.Result.t Future.t


        val where :
          (SqlComposer.Select.t -> SqlComposer.Select.t) ->
          Js.Json.t array ->
          Driver.Connection.t ->
          (Config.t array, exn) Belt.Result.t Future.t

      end

    end

  end


  module Query : sig

    module Read : sig

      val one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t


      val one_by :
        SqlComposer.Select.t ->
        (Js.Json.t -> 'a) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t


      val raw :
        SqlComposer.Select.t ->
        (Js.Json.t -> 'a) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        ('a array, exn) Belt.Result.t Future.t


      val where :
        SqlComposer.Select.t ->
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        (Js.Json.t -> 'a) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        ('a array, exn) Belt.Result.t Future.t

    end


    module Create : sig

      val one :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        ('b -> Js.Json.t) ->
        'b ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t

      
      val batch :
        name:string ->
        table:string ->
        encoder:('a -> Js.Json.t array) ->
        loader:('a array -> ('b array, exn) Belt.Result.t Future.t) ->
        error:(string -> exn) ->
        columns:string array ->
        rows:'a array ->
        Driver.Connection.t ->
        ('b array, exn) Belt.Result.t Future.t

    end


    module Update : sig

      val one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        ('b -> Js.Json.t) ->
        'b ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t


      val increment_one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        string ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t

    end


    module Deactivate : sig

      val one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t

    end


    module Archive : sig

      val one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t


      val compound_by :
        SqlComposer.Select.t ->
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        string ->
        (Js.Json.t -> 'a) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        ('a array, exn) Belt.Result.t Future.t


      val compound_one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a option, exn) Belt.Result.t Future.t

    end


    module Delete :sig

      val one_by_id :
        SqlComposer.Select.t ->
        string ->
        (Js.Json.t -> 'a) ->
        Driver.Id.t ->
        Driver.Connection.t ->
        ('a, exn) Belt.Result.t Future.t

      
      val where :
        SqlComposer.Select.t ->
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        (Js.Json.t -> 'a) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        ('a array, exn) Belt.Result.t Future.t

    end

  end


  module Raw : sig

    val query :
      string ->
      string ->
      Driver.Params.t option ->
      Driver.Connection.t ->
      (Driver.Response.Select.t, exn) Belt.Result.t Future.t


    val query_one :
      string ->
      (Js.Json.t -> 'a) ->
      string ->
      Driver.Params.t option ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t


    val query_many :
      string ->
      (Js.Json.t -> 'a) ->
      string ->
      Driver.Params.t option ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t


    val mutate :
      string ->
      string ->
      Driver.Params.t option ->
      Driver.Connection.t ->
      (Driver.Response.Mutation.t, exn) Belt.Result.t Future.t


    val mutate_batch :
      string ->
      (string -> exn) ->
      string ->
      ('a -> Js.Json.t array) ->
      string array ->
      'a array ->
      Driver.Connection.t ->
      (int, exn) Belt.Result.t Future.t
  end

end
