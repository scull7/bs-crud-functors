
module type Config = sig
  val name: string
end

module Make(Config: Config) = struct
  let debug = Debug.make "bs-crud-functors" Config.name

  external stringify : 'a -> string = "stringify"
  [@@bs.scope "JSON"][@@bs.val]

  let log name sql params =
    let _ = debug "====================" in
    let _ = debug name in
    let _ = debug sql in
    let _ = debug (stringify params) in
    let _ = debug "====================" in
    ()
end
