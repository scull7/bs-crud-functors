(*
 * ## Model
 *)

module type Config = sig
  type t

  val table : string

  val decoder : Js.Json.t -> t

  val base : SqlComposer.Select.t
end

module Factory(Driver: CrudFunctor_driver.Driver) = struct

  module Query = CrudFunctor_query.Make(Driver)

  module Make(Config: Config) = struct
    let base_query = SqlComposer.Select.(Config.base |. from Config.table)
    let decoder = Config.decoder
    let table = Config.table

    module Read = struct

      let one_by_id id = Query.Read.one_by_id base_query table decoder id


      let one_by user_query params =
        let query = base_query |. user_query
        in
        Query.Read.one_by query decoder params


      let raw user_query params =
        let query = base_query |. user_query
        in
        Query.Read.raw query decoder params


      let where user_query params =
        Query.Read.where base_query user_query decoder params

    end


    module Create = struct

      let one encoder record =
        Query.Create.one base_query table decoder encoder record


      let batch ~name ~encoder ~loader ~error ~columns ~rows =
        Query.Create.batch ~name ~table ~encoder ~loader ~error ~columns ~rows

    end


    module Update = struct

      let by_id encoder record id =
        Query.Update.one_by_id base_query table decoder encoder record id


      let increment_one_by_id field id =
        Query.Update.increment_one_by_id base_query table decoder field id
    end


    module Deactivate = struct

      let one_by_id id = Query.Deactivate.one_by_id base_query table decoder id

    end


    module Archive = struct

      let one_by_id id = Query.Archive.one_by_id base_query table decoder id


      let by user_query params =
        Query.Archive.by base_query user_query table decoder params


      let compound_by user_query params =
        Query.Archive.compound_by base_query user_query table decoder params


      let compound_one_by_id id =
        Query.Archive.compound_one_by_id base_query table decoder id

    end


    module Delete = struct

      let where user_query params =
        Query.Delete.where base_query user_query decoder params


      let one_by_id id = Query.Delete.one_by_id base_query table decoder id

    end
  end
end
