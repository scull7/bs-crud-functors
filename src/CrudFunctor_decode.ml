(*
 * ## Decode
 *)
module Exn = CrudFunctor_exn

module Make(Driver: CrudFunctor_driver.Driver) = struct
  let wrap json decoder =
    try
      json |. decoder |. Belt.Result.Ok
    with
    | Json.Decode.DecodeError str ->
      str |. Exn.json_decode_error |. Belt.Result.Error
    | x -> x |. Exn.json_unkown_error |. Belt.Result.Error

  let expected_one count =
    count
    |. string_of_int
    |. (fun x -> {j|Expected: 1, received $x|j})
    |. Exn.UnexpectedRowCount

  let one_row select decoder =
    select
    |. Driver.Response.Select.rows
    |. (fun rows ->
      match (rows |. Belt.Array.length) with
      | 1 ->
          rows
          |. Array.get 0
          |. (wrap decoder)
          |. Belt.Result.map (fun x -> Some x)
      | 0 -> None |. Belt.Result.Ok
      | x -> x |. expected_one |. Belt.Result.Error
    )

  let rows select decoder =
    select
    |. Driver.Response.Select.rows
    |. wrap (fun r -> Belt.Array.map r decoder)
end

(*
 * Date parsing is very weird...
 * If the driver fails to parse the date then an `Invalid Date` object is
 * returned.  Currently, I'm not sure how to detect or handle this.
 *)
external shady_date : 'a -> Js.Date.t = "%identity"

(*
 * If we receive an object then we assume it's a date object.
 *)
let timestamp ts =
  match (ts |. Js.Json.classify) with
  | Js.Json.JSONString str -> str |. Js.Date.fromString |. Some
  | Js.Json.JSONNull -> None
  | Js.Json.JSONObject _ -> (
    let shady_date = shady_date ts in
    match (shady_date |. Js.Date.toString) with
    | "Invalid Date" -> None
    | _ -> Some(shady_date)
  )
  | x -> x |. Js.String.make |. failwith
