(*
 * ## Assert
 *)
module Exn = CrudFunctor_exn
module Select = SqlComposer.Select

let array_not_empty msg array =
  match array with
  | [||] -> Belt.Result.Error (Exn.UnexpectedEmptyArray msg)
  | x -> Belt.Result.Ok x

let has_item maybe msg =
  match maybe with
  | Some x -> Belt.Result.Ok x
  | None -> Belt.Result.Error (Exn.NotFound msg)

let non_empty_user_query msg base user =
  match ((Select.toSql base) == (base |. user |. Select.toSql)) with
  | false -> Belt.Result.Ok user
  | true -> Belt.Result.Error (Exn.EmptyUserQuery msg)
