(*
 * ## Query
 * Userland interface
 *)
open SqlComposer

module Assert = CrudFunctor_assert
module Exn = CrudFunctor_exn

module Make(Driver: CrudFunctor_driver.Driver) = struct

  module Id = Driver.Id
  module Params =CrudFunctor_params.Make(Driver)
  module Raw = CrudFunctor_raw.Make(Driver)
  module Response = Driver.Response

  let error_message name = {j|ERROR: $name failed|j}

  module Read = struct

  let one_by_id base_query table decoder id db =
    Raw.query_one
      "get_one_by_id"
      decoder
      Select.(base_query |. (where {j|AND $table.id = ?|j}) |. toSql)
      (Params.encode_id id)
      db


    let one_by base_query decoder params db =
      Raw.query_one
        "get_one_by"
        decoder
        (base_query |. Select.toSql)
        (params |. Params.positional)
        db


    let raw base_query decoder params db =
      Raw.query_many
        "get"
        decoder
        (base_query |. Select.toSql)
        (params |. Params.positional)
        db


    let where base_query user_query decoder params db =
      Raw.query_many
        "get_where"
        decoder
        ((base_query |. user_query) |. Select.toSql)
        (params |. Params.positional)
        db

    end


  module Create = struct

    let one base_query table decoder encoder record db =
      let sql = {j|INSERT INTO $table SET ?|j} in
      let params = Params.positional [| encoder record |] in
      let get mutation =
        match (Response.Mutation.insertId mutation) with
        | None ->
            "NO_INSERT_ID"
            |. Exn.MutationFailure 
            |. Belt.Result.Error
            |. Future.value
        | Some id -> Read.one_by_id base_query table decoder id db
      in
      Raw.mutate "insert_one" sql params db
      |. Future.flatMapOk get


    let batch ~name ~table ~encoder ~loader ~error ~columns ~rows db =
        Raw.mutate_batch name error table encoder columns rows db
        |. Future.flatMapOk (fun _ -> loader rows)
  end


  module QueryUpdateAbstract = struct

    let by_id name base_query table decoder sql params id db =
      let msg = error_message name in
      let mutation = (fun _ -> Raw.mutate name sql params db)
      in
      Read.one_by_id base_query table decoder id db
      |. Future.flatMapOk (fun x -> Assert.has_item  x msg |. Future.value)
      |. Future.flatMapOk (fun _ -> mutation db)
      |. Future.flatMapOk (fun _ ->
          Read.one_by_id base_query table decoder id db
      )

    let action_by_id name base_query table decoder action_sql id db =
      let params = [| Id.toJson id |] |. Params.positional
      in
      by_id name base_query table decoder action_sql params id db
  end


  module Update = struct

  let one_by_id base_query table decoder encoder record id db =
    let name = "update_one_by_id" in
    let sql = {j|UPDATE $table SET ? WHERE $table.`id` = ?|j} in
    let params = [| (encoder record); (Id.toJson id); |] |. Params.positional
    in
    QueryUpdateAbstract.by_id name base_query table decoder sql params id db


    let increment_one_by_id base_query table decoder field id db =
      let name = "increment_one_by_id" in
      let sql =
        SqlComposer.Update.(
          make()
          |. from table
          |. set field {j| $field + 1 |j}
          |. where {j|AND $table.`id` = ?|j}
          |. toSql
        )
      in
      QueryUpdateAbstract.action_by_id name base_query table decoder sql id db

  end


  module Deactivate = struct

    let one_by_id base_query table decoder id db =
      let name = "deactivate_one_by_id" in
      let sql =
        SqlComposer.Update.(
          make()
          |. from table
          |. set {j|$table.`active`|j} "0"
          |. where {j|AND $table.`id` = ?|j}
          |. toSql
        )
      in
      QueryUpdateAbstract.action_by_id name base_query table decoder sql id db

  end


  module Archive = struct
    let one_by_id base_query table decoder id db =
      let name = "archive_one_by_id" in
      let sql =
        SqlComposer.Update.(
          make()
          |. from table
          |. set {j|$table.`deleted`|j} "1"
          |. where {j|AND $table.`id` = ?|j}
          |. toSql
        )
      in
      QueryUpdateAbstract.action_by_id name base_query table decoder sql id db


    let by base_query user_query table decoder params db =
      let name = "archive_by" in
      let user_select = user_query base_query in
      let params2 = Params.positional params in
      let msg = error_message name in
      let sql =
        Conversion.updateFromSelect user_select
        |. SqlComposer.Update.set
            {j|$table.`deleted`|j}
            "UNIX_TIMESTAMP(NOW())"
        |. SqlComposer.Update.toSql
      in
      Assert.non_empty_user_query msg base_query user_query
      |. Future.value
      |. Future.flatMapOk (fun x -> Read.where base_query x decoder params db)
      |. Future.flatMapOk (fun x ->
          (Assert.array_not_empty msg x) |. Future.value
      )
      |. Future.flatMapOk (fun _ -> Raw.mutate name sql params2 db)
      |. Future.flatMapOk (fun _ ->
          Read.where base_query user_query decoder params db
      )


    let compound_by base_query user_query table decoder params db =
      let name = "archive_compound_by" in
      let user_select = user_query base_query in
      let params2 = Params.positional params in
      let msg = error_message name in
      let sql =
        Conversion.updateFromSelect user_select
        |. SqlComposer.Update.set {j|$table.`deleted`|j} "1"
        |. SqlComposer.Update.set
            {j|$table.`deleted_timestamp`|j}
            "NOW()"
        |. SqlComposer.Update.toSql
      in
      Assert.non_empty_user_query msg base_query user_query
      |. Future.value
      |. Future.flatMapOk (fun x -> Read.where base_query x decoder params db)
      |. Future.flatMapOk (fun x ->
          (Assert.array_not_empty msg x) |. Future.value
      )
      |. Future.flatMapOk (fun _ -> Raw.mutate name sql params2 db)
      |. Future.flatMapOk (fun _ ->
          Read.where base_query user_query decoder params db
      )


    let compound_one_by_id base_query table decoder id db =
      let name = "archive_compound_one_by_id" in
      let sql =
        SqlComposer.Update.(
          make()
          |. from table
          |. set {j|$table.`deleted`|j} "1"
          |. set {j|$table.`deleted_timestamp`|j} "NOW()"
          |. where {j|AND $table.`id` = ?|j}
          |. toSql
        )
      in
      QueryUpdateAbstract.action_by_id name base_query table decoder sql id db
  end

  module Delete = struct
    let where base_query user_query decoder params db =
      let name = "Delete.where" in
      let msg = error_message name in
      let params2 = Params.positional params in
      let sql =
        base_query
        |. user_query
        |. Conversion.deleteFromSelect
        |. SqlComposer.Delete.toSql
      in
      Assert.non_empty_user_query msg base_query user_query
      |. Future.value
      |. Future.flatMapOk (fun x -> Read.where base_query x decoder params db)
      |. Future.flatMapOk (fun x ->
          (Assert.array_not_empty msg x) |. Future.value
      )
      |. Future.flatMapOk (fun x ->
          Raw.mutate name sql params2 db
          |. Future.mapOk (fun _ -> x)
      )


    let one_by_id base_query table decoder id db =
      let name = "Delete.one_by_id" in
      let msg = error_message name in
      let params = Params.encode_id id in
      let sql =
        SqlComposer.Delete.make ()
        |. SqlComposer.Delete.from table
        |. SqlComposer.Delete.where {j|AND $table.`id` = ?|j}
        |. SqlComposer.Delete.toSql
      in
      Read.one_by_id base_query table decoder id db
      |. Future.flatMapOk (fun x -> x |. Assert.has_item msg |. Future.value)
      |. Future.flatMapOk (fun x ->
        Raw.mutate name sql params db
        |. Future.mapOk(fun _ -> x)
      )
  end

end
