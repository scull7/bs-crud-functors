(*
 * ## Query Interface
 * Userland interface
 *)

module Make(Driver: CrudFunctor_driver.Driver) : sig

  module Read : sig

    val one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t


    val one_by :
      SqlComposer.Select.t ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t


    val raw :
      SqlComposer.Select.t ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t


    val where :
      SqlComposer.Select.t ->
      (SqlComposer.Select.t -> SqlComposer.Select.t) ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t

  end


  module Create : sig

    val one :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      ('b -> Js.Json.t) ->
      'b ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t

    
    val batch :
      name:string ->
      table:string ->
      encoder:('a -> Js.Json.t array) ->
      loader:('a array -> ('b array, exn) Belt.Result.t Future.t) ->
      error:(string -> exn) ->
      columns:string array ->
      rows:'a array ->
      Driver.Connection.t ->
      ('b array, exn) Belt.Result.t Future.t

  end


  module Update : sig

    val one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      ('b -> Js.Json.t) ->
      'b ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t


    val increment_one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      string ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t

  end


  module Deactivate : sig

    val one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t

  end


  module Archive : sig

    val one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t


    val by :
      SqlComposer.Select.t ->
      (SqlComposer.Select.t -> SqlComposer.Select.t) ->
      string ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t


    val compound_by :
      SqlComposer.Select.t ->
      (SqlComposer.Select.t -> SqlComposer.Select.t) ->
      string ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t


    val compound_one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a option, exn) Belt.Result.t Future.t

  end


  module Delete :sig

    val one_by_id :
      SqlComposer.Select.t ->
      string ->
      (Js.Json.t -> 'a) ->
      Driver.Id.t ->
      Driver.Connection.t ->
      ('a, exn) Belt.Result.t Future.t

    
    val where :
      SqlComposer.Select.t ->
      (SqlComposer.Select.t -> SqlComposer.Select.t) ->
      (Js.Json.t -> 'a) ->
      Js.Json.t array ->
      Driver.Connection.t ->
      ('a array, exn) Belt.Result.t Future.t

  end

end
