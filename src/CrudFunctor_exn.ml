exception EmptyUserQuery of string

exception MutationFailure of string

exception NotFound of string

exception QueryFailure of string

exception UnexpectedEmptyArray of string

exception JsonDecodeError of string

exception JsonDecodeUnknownError of string

exception UnexpectedRowCount of string

let mutation exn =
  exn |. Js.String.make |. MutationFailure

let select exn =
  exn |. Js.String.make |. QueryFailure

let json_decode_error str = str |. JsonDecodeError 

let json_unkown_error x = x |. Js.String.make |. JsonDecodeUnknownError
