module type Driver = sig
  
  module Connection : sig
    type t
  end

  module Id : sig
    type t

    val fromJson : Js.Json.t -> t

    val toJson : t -> Js.Json.t

    val toString : t -> string
  end

  module Params : sig
    type t

    val named : Js.Json.t -> t

    val positional : Js.Json.t -> t
  end

  module Response : sig
    module Mutation : sig
      type t

      val insertId : t -> Id.t option

      val affectedRows : t -> int
    end

    module Select : sig
      type t

      val rows : t -> Js.Json.t array
    end
  end

  module Promise : sig
    val mutate :
      db: Connection.t ->
      ?params:Params.t ->
      sql:string ->
      Response.Mutation.t Js.Promise.t

    val query :
      db:Connection.t ->
      ?params:Params.t ->
      sql:string ->
      Response.Select.t Js.Promise.t

    module Batch : sig
      val mutate :
        db:Connection.t ->
        ?batch_size:int ->
        table:string ->
        columns:string array ->
        encoder:('a -> Js.Json.t array) ->
        rows:'a array ->
        unit ->
        int Js.Promise.t
    end
  end

end
