(*
 * ## Model Interface
 *)

module type Config = sig
  type t

  val table : string

  val decoder : Js.Json.t -> t

  val base : SqlComposer.Select.t
end


module Factory(Driver: CrudFunctor_driver.Driver) : sig

  module Make(Config: Config) : sig

    module Read : sig
      val one_by_id :
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t


      val one_by :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t


      val raw :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t array, exn) Belt.Result.t Future.t


      val where :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t array, exn) Belt.Result.t Future.t
    end


    module Create : sig

      val one :
        ('a -> Js.Json.t) ->
        'a ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t


      val batch :
        name:string ->
        encoder:('a -> Js.Json.t array) ->
        loader:('a array -> ('b array, exn) Belt.Result.t Future.t) ->
        error:(string -> exn) ->
        columns:string array ->
        rows:'a array ->
        Driver.Connection.t ->
        ('b array, exn) Belt.Result.t Future.t
    end


    module Update : sig

      val by_id :
        ('a -> Js.Json.t) ->
        'a ->
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t


      val increment_one_by_id :
        string ->
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t
    end


    module Deactivate : sig

      val one_by_id :
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t

    end


    module Archive : sig

      val one_by_id :
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t


      val by :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t array, exn) Belt.Result.t Future.t


      val compound_by :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t array, exn) Belt.Result.t Future.t


      val compound_one_by_id :
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t option, exn) Belt.Result.t Future.t

    end


    module Delete : sig

      val one_by_id :
        Driver.Id.t ->
        Driver.Connection.t ->
        (Config.t, exn) Belt.Result.t Future.t


      val where :
        (SqlComposer.Select.t -> SqlComposer.Select.t) ->
        Js.Json.t array ->
        Driver.Connection.t ->
        (Config.t array, exn) Belt.Result.t Future.t

    end
  end
end
