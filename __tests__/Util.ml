
module Driver = SqlCommon.Make(MySql2)


module CF = CrudFunctor.Make(Driver)

module ModelFactory = CrudFunctor_model.Factory(Driver)

module Future = struct

  let test name runner =
    Jest.testPromise name (fun _ ->
      Js.Promise.make (fun ~resolve ~reject ->
        runner()
        |. Future.get (fun result ->
          match result with
          | Belt.Result.Ok x -> resolve x [@bs]
          | Belt.Result.Error e -> reject e [@bs]
        )
        |. ignore
      )
    )

end


module Env = struct

  let get_with_default key default =
    Node.Process.process##env
    |. Js.Dict.get key
    |. Belt.Option.getWithDefault default

end


module Connection = struct

  let connect _ =
    Driver.Connection.connect
      ~host:(Env.get_with_default "JEST_MYSQL_HOST" "127.0.0.1")
      ~port:(Env.get_with_default "JEST_MYSQL_PORT" "3306" |. int_of_string)
      ~user:(Env.get_with_default "JEST_MYSQL_USER" "root")
      ()
end


module Init = struct

  let mutate debug db str sql =
    let _ = debug str in
    Driver.Promise.mutate ~db ?params:None ~sql


  let drop_db debug db database_name =
    mutate
      debug
      db
      {j| drop database - $database_name |j}
      {j| DROP DATABASE IF EXISTS $database_name |j}


  let create_db debug db database_name =
    mutate
      debug
      db
      {j| create database - $database_name |j}
      {j| CREATE DATABASE IF NOT EXISTS $database_name |j}


  let use_db debug db database_name =
    mutate
      debug
      db
      {j| use database - $database_name |j}
      {j| USE $database_name |j}


  let run debug db database_name =
    drop_db debug db database_name
    |> Js.Promise.then_ (fun _ -> create_db debug db database_name)
    |> Js.Promise.then_ (fun _ -> use_db debug db database_name)
    |> Js.Promise.then_ (fun _ -> db |. Js.Promise.resolve)
end
