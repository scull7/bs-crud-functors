open Jest

module Exn = CrudFunctor_exn

let () =

describe "CrudFunctor_exn" (fun _ ->

  describe "json_decode_error" (fun _ ->

    test "should return a JsonDecodeError" (fun _ ->
      match Exn.json_decode_error "yay!" with
      | Exn.JsonDecodeError str -> str |> Expect.expect |> Expect.toBe "yay!"
      | e -> e |> Js.String.make |> fail
    )
  )
);
