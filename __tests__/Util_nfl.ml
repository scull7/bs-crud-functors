open CrudFunctor.Sql

module Team = struct
  type team = {
    id: Util.CF.Id.t;
    city: string;
    nickname: string;
    likes: int;
    active: int;
    deleted: int;
    deleted_timestamp: Js.Date.t option;
  }

  module New = struct
    type t = {
      city: string;
      nickname: string;
    }

    let make city nickname = { city; nickname; }

    let to_json {city; nickname; } = Json.Encode.(object_ [
      ("city", string city);
      ("nickname", string nickname);
    ])
  end

  let table = "TEST_CF_MODEL_NFL"

  module Sql = struct
    let table = "TEST_CF_MODEL_NFL"

    let create = {j|
      CREATE TABLE $table (
        id bigint(20) NOT NULL AUTO_INCREMENT
      , city varchar(32) NOT NULL
      , nickname varchar(32) NOT NULL
      , likes int(11) NOT NULL DEFAULT 0
      , active tinyint(1) NOT NULL DEFAULT 1
      , deleted tinyint(1) NOT NULL DEFAULT 0
      , deleted_timestamp timestamp(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000'
      , PRIMARY KEY (id)
      , UNIQUE (nickname)
      )
    |j}

    let seed = {j|
      INSERT INTO $table (city, nickname)
      VALUES
        ('pittsburgh', 'steelers')
      , ('san francisco', '49ers')
      , ('indianapolis', 'colts')
      , ('new orleans', 'saints')
      , ('carolina', 'panthers')
    |j}

    let init mutate =
      mutate {j| create table - $table |j} create
      |> Js.Promise.then_ (fun _ -> mutate {j| seed table - $table |j} seed)

  end

  include Util.ModelFactory.Make(struct
    type t = team

    let table = Sql.table

    let timestamp = CrudFunctor_decode.timestamp

    let decoder json = Json.Decode.{
      id = json |> field "id" Util.CF.Id.fromJson;
      city = json |> field "city" string;
      nickname = json |> field "nickname" string;
      likes = json |> field "likes" int;
      active = json |> field "active" int;
      deleted = json |> field "deleted" int;
      deleted_timestamp = json |> field "deleted_timestamp" timestamp
    }

    let base =
      Select.(
        make ()
        |. field {j|$table.id|j}
        |. field {j|$table.city|j}
        |. field {j|$table.nickname|j}
        |. field {j|$table.likes|j}
        |. field {j|$table.active|j}
        |. field {j|$table.deleted|j}
        |. field {j|$table.deleted_timestamp|j}
        |. orderBy (`Desc {j|$table.id|j})
      )
  end)

  let insert city nickname db =
    Create.one
      New.to_json
      (New.make city nickname)
      db

  let get_by_nickname nickname db =
    Read.one_by
      (fun base -> base |. Select.where {j| AND $table.nickname = ? |j})
      Json.Encode.([| string nickname |])
      db

  let get_by_nickname_list nicknames db =
    Read.where
      (fun base -> base |. Select.where {j| AND $table.nickname IN(?) |j})
      (
        nicknames
        |. Belt.Array.map Json.Encode.string
        |. Json.Encode.jsonArray
        |. (fun a -> [| a |])
      )
      db

  let set_likes_by_nickname nickname likes db =
    get_by_nickname nickname db
    |. Future.mapOk Belt.Option.getExn
    |. Future.flatMapOk (fun ({ id }) ->
      let encoder x = Json.Encode.(object_ [ ("likes", int x); ])
      in
      Update.by_id encoder likes id db
    )

  let insert_batch teams db =
    Create.batch
      ~name:"NFL.Team.insert_batch"
      ~encoder:(fun (city, nickname) ->
        Json.Encode.([| (string city); (string nickname); |])
      )
      ~loader:(fun a ->
        Belt.Array.map a (fun (_, nickname) -> nickname)
        |. get_by_nickname_list db
      )
      ~error:(fun str -> CrudFunctor_exn.MutationFailure str)
      ~columns:[| "city"; "nickname"; |]
      ~rows:teams
      db

    let archive_by_like_count likes db =
      Archive.compound_by
        (fun base -> base |. Select.where {j| AND $table.likes = ? |j})
        [| likes |. Json.Encode.int |]
        db

    let delete_by_like_count likes db =
      Delete.where
        (fun base -> base |. Select.where {j| AND $table.likes = ? |j})
        [| likes |. Json.Encode.int |]
        db

end
