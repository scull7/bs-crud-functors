open Util.CF.Query


type t = {
  id: Util.CF.Id.t;
  category: string;
  deleted: int;
  deleted_timestamp: Js.Date.t option;
}

module New = struct
  type t = { category: string; }

  let category { category } = category

  let make category = { category }

  let to_json { category } =
    Json.Encode.([ ("category", string category) ] |> object_)

  let to_json_deleted { category } =
    Json.Encode.(
      [
        ("category", string category);
        ("deleted", int 1);
      ]
      |. object_
    )

  let to_json_bad_field { category } =
    Json.Encode.(
      [ "bad_column", string category ] |. object_
    )
end


let id t = t.id


let category t = t.category


module Decode = CrudFunctor_decode

let decoder json =
  Json.Decode.{
    id = json |> field "id" Util.CF.Id.fromJson;
    category = json |> field "category" string;
    deleted = json |> field "deleted" int;
    deleted_timestamp = json |> field "deleted_timestamp" Decode.timestamp
  }

module Sql = struct

  module Select = CrudFunctor.Sql.Select

  let table = "TEST_CF_QUERY_ANIMAL"

  let create = {j|
    CREATE TABLE $table (
      id bigint(20) NOT NULL AUTO_INCREMENT,
      category varchar(16) NOT NULL,
      deleted tinyint(1) NOT NULL DEFAULT 0,
      deleted_timestamp timestamp(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000',
      PRIMARY KEY (id),
      UNIQUE (category)
    )
  |j}

  let seed = {j|
    INSERT INTO $table (category)
    VALUES
    ('dog'), ('cat'), ('elephant'), ('dogfish'), ('moose');
  |j}


  let init mutate =
    mutate {j| create table $table|j} create
    |> Js.Promise.then_ (fun _ -> mutate {j| seed table $table|j} seed)


  let base = CrudFunctor.Sql.Select.(
    make ()
    |. field "*"
    |. from table
  )


  let get_by_category =
    Select.(base |. where {j|AND $table.`category` = ?|j})


  let where_by_category base =
    Select.(base |. where {j|AND $table.`category` = ?|j})


  let where_by_category_invalid base =
    Select.(base |. where {j|AND $table.`category` ?|j})


  let where_not_deleted base =
    Select.(base |. where {j|AND $table.`deleted` = 0|j})


  let where_category_in base =
    Select.(base |. where {j|AND $table.`category` IN(?)|j})


  let insert db category =
    let record = New.make category in
    Create.one base table decoder New.to_json record db


  let update db category id =
    let record = New.make category in
    Update.one_by_id base table decoder New.to_json record id db


  let update_no_result db category id =
    let base = base |. Select.where {j|AND $table.`deleted` = 0|j} in
    let record = New.make category in
    let encoder x = Json.Encode.(
      [
        ("category", x |. New.category |. string);
        ("deleted", int 1);
      ]
      |> object_
    )
    in
    Update.one_by_id base table decoder encoder record id db


  let update_bad_column db category id =
    let record = New.make category in
    let encoder = New.to_json_bad_field in
    Update.one_by_id base table decoder encoder record id db


  let archive_compound_by db category =
    let where base = base |. Select.where {j|AND $table.`category` = ?|j} in
    let params = Json.Encode.([| string category |]) in
    Archive.compound_by base where table decoder params db


  let archive_compound_by_no_result db category =
    let base = base |. Select.where {j|AND $table.`deleted` = 0|j} in
    let where base = base |. Select.where {j|AND $table.`category` = ?|j} in
    let params = Json.Encode.([| string category |]) in
    Archive.compound_by base where table decoder params db


  let archive_compound_by_empty db category =
    let where base = base in
    let params = Json.Encode.([| string category |]) in
    Archive.compound_by base where table decoder params db


  let archive_compound_one_by_id db id =
    Archive.compound_one_by_id base table decoder id db


  let archive_compound_one_by_id_no_result db id =
    let base = base |. Select.where {j|AND $table.`deleted` = 0|j} in
    Archive.compound_one_by_id base table decoder id db

  let delete_one_by_id db id =
    Delete.one_by_id base table decoder id db
end
