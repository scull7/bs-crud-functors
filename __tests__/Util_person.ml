open Util.CF.Query

type t = {
  id: Util.CF.Id.t;
  first_name: string;
  age: int;
  active: int;
  deleted: int;
}

let idGet { id } = id

let ageGet { age } = age

let decoder json = Json.Decode.{
  id = json |> field "id" Util.CF.Id.fromJson;
  first_name = json |> field "first_name" string;
  age = json |> field "age" int;
  active = json |> field "active" int;
  deleted = json |> field "deleted" int;
}

module New = struct

  type t = {
    first_name: string;
    age: int;
  }

  let make first_name age = { first_name; age; }

  let to_json { first_name; age } = Json.Encode.(object_ [
    ("first_name", string first_name);
    ("age", int age);
  ])

end


module Sql = struct

  module Select = CrudFunctor.Sql.Select

  let table = "TEST_CF_QUERY_PERSON"

  let base = Select.(make () |. field "*" |. from table)

  module Init = struct
    let create = {j|
      CREATE TABLE $table (
        id mediumint NOT NULL AUTO_INCREMENT
      , first_name varchar(120) NOT NULL
      , age tinyint(3) UNSIGNED NOT NULL
      , active tinyint(1) NOT NULL DEFAULT 1
      , deleted int(10) UNSIGNED NOT NULL DEFAULT 0
      , PRIMARY KEY (id)
      , UNIQUE (first_name)
      )
    |j}

    let seed = {j|
      INSERT INTO $table
      (first_name, age)
      VALUES
      ('gayle', 28), ('patrick', 65), ('cody', 29), ('clinton', 27);
    |j}

    let run mutate =
      mutate {j| create table $table|j} create
      |> Js.Promise.then_ (fun _ -> mutate {j| seed table $table|j} seed)
  end

    let get_by_id db id =
      Read.one_by_id base table decoder id db
  
    let insert db name age =
      let record = New.make name age in
      Create.one base table decoder New.to_json record db

    let increment_age db id =
      Update.increment_one_by_id base table decoder "age" id db

    let increment_age_no_result db age id =
      let base = base |. Select.where {j|AND $table.age = $age|j} in
      Update.increment_one_by_id base table decoder "age" id db

    let increment_bad_field db id =
      Update.increment_one_by_id base table decoder "bad_column" id db

    let deactivate_one_by_id db id =
      Deactivate.one_by_id base table decoder id db

    let deactivate_one_by_id_no_result db id =
      let base = base |. Select.where {j|AND $table.active = 1|j} in
      Deactivate.one_by_id base table decoder id db

    let deactivate_one_by_id_not_found db id =
      let base = base |. Select.where {j|AND $table.active = 0|j} in
      Deactivate.one_by_id base table decoder id db

    let archive_one_by_id db id =
      Archive.one_by_id base table decoder id db

    let archive_one_by_id_no_result db id =
      let base = base |. Select.where {j|AND $table.deleted = 0|j} in
      Archive.one_by_id base table decoder id db

    let archive_one_by_id_not_found db id =
      let base = base |. Select.where {j|AND $table.deleted = 1|j} in
      Archive.one_by_id base table decoder id db

    let delete_where db age =
      let where base = base |. Select.where {j| AND $table.age = ? |j} in
      let params = [| Json.Encode.int age |] in
      Delete.where base where decoder params db

    let delete_where_empty db age =
      let where base = base in
      let params = [| Json.Encode.int age |] in
      Delete.where base where decoder params db
end
