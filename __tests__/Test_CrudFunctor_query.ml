open Jest

let debug = Debug.make "bs-crud-functors" "test:CrudFunctor_decode"

module CF = Util.CF
open CF.Query

module Exn = CrudFunctor.Exn

module Animal = Util_animal

module Person = Util_person

let db = Util.Connection.connect ()

let database_name = "TEST_CF_QUERY"

let mutate = Util.Init.mutate debug db

let () =

describe "CrudFunctor_query" (fun _ ->

  let _ = beforeAllPromise (fun _ ->
    Util.Init.run debug db database_name
    |> Js.Promise.then_ (fun _ -> Animal.Sql.init mutate)
    |> Js.Promise.then_ (fun _ -> Person.Sql.Init.run mutate)
  )
  in
  let _ = afterAll (fun _ -> Util.Driver.Connection.close db)
  in

  describe "Read.one_by_id" (fun _ ->

    Util.Future.test "should return one result" (fun _ ->
      CF.Query.Read.one_by_id
        Animal.Sql.base
        Animal.Sql.table
        Animal.decoder
        (CF.Id.fromJson (Js.Json.string "3"))
        db
      |. Future.mapOk (fun a -> a |. Belt.Option.getExn |. Animal.category)
      |. Future.mapOk (fun c -> c |> Expect.expect |> Expect.toBe "elephant")
    );

    Util.Future.test "does not return anything" (fun _ ->
      Read.one_by_id
        Animal.Sql.base
        Animal.Sql.table
        Animal.decoder
        (CF.Id.fromJson (Js.Json.string "100"))
        db
      |. Future.mapOk (fun maybe ->
        match maybe with
        | Some _ -> "UNEXPECTED_RESULT" |. fail
        | None -> pass
      )
    )
  );

  describe "Read.one_by" (fun _ ->

    Util.Future.test "should return one result" (fun _ ->
      Read.one_by
        Animal.Sql.get_by_category
        Animal.decoder
        [| Json.Encode.string "cat" |]
        db
      |. Future.mapOk (fun a -> a |. Belt.Option.getExn |. Animal.category)
      |. Future.mapOk (fun c -> c |> Expect.expect |> Expect.toBe "cat")
    );

    Util.Future.test "does not return anything" (fun _ ->
      Read.one_by
        Animal.Sql.get_by_category
        Animal.decoder
        [| Json.Encode.string "tree" |]
        db
      |. Future.mapOk (fun maybe ->
        match maybe with
        | Some _ -> "UNEXPECTED_RESULT" |. fail
        | None -> pass
      )
    )
  );

  describe "Read.raw" (fun _ ->

    Util.Future.test "should return one result" (fun _ ->
      Read.raw
        Animal.Sql.get_by_category
        Animal.decoder
        [| Json.Encode.string "moose" |]
        db
      |. Future.mapOk (fun a -> a |. Belt.Array.getExn 0)
      |. Future.mapOk (fun a -> a |. Animal.category)
      |. Future.mapOk (fun c -> c |> Expect.expect |> Expect.toBe "moose")
    );

    Util.Future.test "does not return anything" (fun _ ->
      Read.raw
        Animal.Sql.get_by_category
        Animal.decoder
        [| Json.Encode.string "truck" |]
        db
      |. Future.mapOk (fun a -> a |. Belt.Array.length)
      |. Future.mapOk (fun l -> l |> Expect.expect |> Expect.toBe 0)
    )
  );

  describe "Read.where" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Read.where
        Animal.Sql.base
        Animal.Sql.where_by_category
        Animal.decoder
        [| Json.Encode.string "dog" |]
        db
      |. Future.mapOk (fun a -> a |. Belt.Array.getExn 0)
      |. Future.mapOk (fun a -> a |. Animal.category)
      |. Future.mapOk (fun c -> c |> Expect.expect |> Expect.toBe "dog")
    );

    Util.Future.test "does not reutrn anything" (fun _ ->
      Read.where
        Animal.Sql.base
        Animal.Sql.where_by_category
        Animal.decoder
        [| Json.Encode.string "couch" |]
        db
      |. Future.mapOk (fun a -> a |. Belt.Array.length)
      |. Future.mapOk (fun l -> l |> Expect.expect |> Expect.toBe 0)
    );

    testAsync "fails and throws a syntax error" (fun done_ ->
      Read.where
        Animal.Sql.base
        Animal.Sql.where_by_category_invalid
        Animal.decoder
        [| Json.Encode.string "cat" |]
        db
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    )
  );

  describe "Insert.one" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Create.one
        Animal.Sql.base
        Animal.Sql.table
        Animal.decoder
        Animal.New.to_json
        (Animal.New.make "pangolin")
        db
      |. Future.mapOk (fun r ->
        match r with
        | Some { category = "pangolin" } -> pass
        | _ -> "UNEXPECTED_RESULT" |. fail
      )
    );

    Util.Future.test "return no result" (fun _ ->
      Create.one
        (Animal.Sql.where_not_deleted Animal.Sql.base)
        Animal.Sql.table
        Animal.decoder
        Animal.New.to_json_deleted
        (Animal.New.make "turkey")
        db
      |. Future.mapOk (fun r ->
        match r with
        | None -> pass
        | Some _ -> "UNEXPECTED_RESULT" |. fail
      )
    );

    testAsync "fails with a unique constraint error" (fun done_ ->
      Create.one
        Animal.Sql.base
        Animal.Sql.table
        Animal.decoder
        Animal.New.to_json
        (Animal.New.make "elephant")
        db
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    );

    testAsync "fails with a bad field error" (fun done_ ->
      Create.one
        Animal.Sql.base
        Animal.Sql.table
        Animal.decoder
        Animal.New.to_json_bad_field
        (Animal.New.make "flamingo")
        db
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    )
  );

  describe "Insert.batch" (fun _ ->

    Util.Future.test "returns 2 results" (fun _ ->
      let expected = [| "catfish"; "lumpsucker"; |] in
      let extract_output rows =
        Belt.Array.map rows Animal.category
      in
      let extract_input rows =
        rows
        |. Belt.Array.map Json.Encode.string
        |. Json.Encode.jsonArray
        |. (fun a -> [| a; |])
      in
      let loader rows =
        Read.where
          Animal.Sql.base
          Animal.Sql.where_category_in
          Animal.decoder
          (extract_input rows)
          db
      in
      Create.batch
        ~name:"Insert.batch returns 2 results"
        ~table:Animal.Sql.table
        ~encoder:(fun x -> [| Js.Json.string x |])
        ~loader
        ~error:(fun err -> err |. Exn.MutationFailure)
        ~columns:[| "category" |]
        ~rows:expected
        db
      |. Future.mapOk (fun r -> r |. extract_output)
      |. Future.mapOk (fun r -> r |> Expect.expect |> Expect.toEqual  expected)
    );

    testAsync "fails with unique constraint error" (fun done_ ->
      Create.batch
        ~name:"fails with unique constraint error"
        ~table:Animal.Sql.table
        ~encoder:(fun x -> [| Js.Json.string x |])
        ~loader: (fun x -> x |. Belt.Result.Ok |. Future.value)
        ~error:(fun e -> e |. Exn.MutationFailure)
        ~columns:[| "category" |]
        ~rows:[| "dog"; "cat"; |]
        db
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    );

    Util.Future.test "given en empty array returns an empty array" (fun _ ->
      Create.batch
        ~name:"fails with unique constraint error"
        ~table:Animal.Sql.table
        ~encoder:(fun x -> [| Js.Json.string x |])
        ~loader: (fun x -> x |. Belt.Result.Ok |. Future.value)
        ~error:(fun e -> e |. Exn.MutationFailure)
        ~columns:[| "category" |]
        ~rows:[||]
        db
      |. Future.mapOk (fun x -> x |> Expect.expect |> Expect.toEqual [||])
    )
  );

  describe "Update.by_id" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Animal.Sql.insert db "honey badgder"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (fun id -> Animal.Sql.update db "honey badger" id)
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.category)
      |. Future.mapOk (fun x -> x |> Expect.expect |> Expect.toBe "honey badger")
    );

    Util.Future.test "succeeds but returns no results" (fun _ ->
      Animal.Sql.insert db "flamenco"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (fun id ->
          Animal.Sql.update_no_result db "flamingo" id
      )
      |. Future.mapOk (fun r ->
          match r with
          | None -> pass
          | Some _ -> "UNEXPECTED_RESULT" |. fail
      )
    );

    testAsync "fails and returns not found" (fun done_ ->
      Animal.Sql.update db "dodo" ("999" |. Js.Json.string |. CF.Id.fromJson)
      |. Future.map (fun x ->
          match x with
          | Belt.Result.Error CrudFunctor.Exn.NotFound _ -> pass |. done_
          | _ -> "UNEXPECTED_RESULT" |. fail |. done_
      )
      |. ignore
    );

    testAsync "fails and throws a bad field error" (fun done_ ->
      Animal.Sql.insert db "hipppopotamus"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (fun id ->
          Animal.Sql.update_bad_column db "hippopotamus" id
      )
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    )
  );

  describe "Update.increment_one_by_id" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Person.Sql.insert db "nate" 37
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.increment_age db)
      |. Future.mapOk (fun p ->
          match p with
          | Some {
              first_name = "nate";
              age = 38;
              active = 1;
              deleted = 0;
              _
            } -> pass
          | x -> x |. Js.String.make |. fail
      )
    );

    Util.Future.test "succeeds but returns no result" (fun _ ->
      Person.Sql.insert db "adam" 127
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.increment_age_no_result db 127)
      |. Future.mapOk (fun x ->
          match x with
          | None -> pass
          | Some x -> x |. Js.String.make |. fail
      )
    );

    testAsync "fails and returns not found" (fun done_ ->
      Person.Sql.insert db "piotr" 30
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.increment_age_no_result db 50)
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun e ->
          match e with
          | Exn.NotFound _ -> pass |. done_
          | x -> x |. Js.String.make |. fail |. done_
      )
      |. ignore
    );

    testAsync "fails and throws a bad field error" (fun done_ ->
      Person.Sql.insert db "sean" 33
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.increment_bad_field db)
      |. Future.mapOk (fun _ -> "UNEXPECTED_RESULT" |. fail |. done_)
      |. Future.mapError (fun _ -> pass |. done_)
      |. ignore
    )
  );

  describe "Deactivate.one_by_id" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Person.Sql.insert db "tj" 27
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.deactivate_one_by_id db)
      |. Future.mapOk (fun p ->
        match p with
        | Some { first_name = "tj"; age = 27; active = 0; deleted = 0; _ } ->
            pass
        | Some e -> e |. Js.String.make |. fail
        | None -> "UNEXPECTED_NOT_FOUND" |. fail
      )
    );

    Util.Future.test "succeeds but returns no result" (fun _ ->
      Person.Sql.insert db "jon" 24
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.deactivate_one_by_id_no_result db)
      |. Future.mapOk (fun x ->
          match x with
          | Some x -> x |. Js.String.make |. fail
          | None -> pass
      )
    );

    testAsync "fails and returns NotFound" (fun done_ ->
      Person.Sql.insert db "Max" 69
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.deactivate_one_by_id_not_found db)
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail |. done_)
      |. Future.mapError (fun e ->
          match e with
          | Exn.NotFound _ -> pass |. done_
          | e -> e |. Js.String.make |. fail |. done_
      )
      |. ignore
    )
  );

  describe "Archive.one_by_id" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Person.Sql.insert db "aaron" 28
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.archive_one_by_id db)
      |. Future.mapOk (fun p ->
        match p with
        | Some { first_name = "aaron"; age = 28; active = 1; deleted = 1; _ } ->
            pass
        | Some e -> e |. Js.String.make |. fail
        | None -> "UNEXPECTED_NOT_FOUND" |. fail
      )
    );

    Util.Future.test "succeeds but returns no result" (fun _ ->
      Person.Sql.insert db "lucho" 24
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.archive_one_by_id_no_result db)
      |. Future.mapOk (fun x ->
          match x with
          | Some x -> x |. Js.String.make |. fail
          | None -> pass
      )
    );

    testAsync "fails and returns NotFound" (fun done_ ->
      Person.Sql.insert db "stephen" 34
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Person.idGet)
      |. Future.flatMapOk (Person.Sql.archive_one_by_id_not_found db)
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail |. done_)
      |. Future.mapError (fun e ->
          match e with
          | Exn.NotFound _ -> pass |. done_
          | e -> e |. Js.String.make |. fail |. done_
      )
      |. ignore
    )
  );

  describe "Archive.compound_by" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Animal.Sql.insert db "tiger"
      |. Future.flatMapOk (fun _ -> Animal.Sql.archive_compound_by db "tiger")
      |. Future.mapOk (fun p ->
        match p with
        | [| { category = "tiger"; deleted = 1; deleted_timestamp = ts; _ } |] ->
          (match ts with
          | None -> "UNEXPECTED_NOT_FOUND" |. fail
          | Some _ -> pass
          )
        | e -> e |. Js.String.make |. fail
      )
    );

    Util.Future.test "succeeds but returns no results" (fun _ ->
      Animal.Sql.insert db "leopard"
      |. Future.flatMapOk (fun _ ->
          Animal.Sql.archive_compound_by_no_result db "leopard"
      )
      |. Future.mapOk (fun p ->
        match p with
        | [||] -> pass
        | x -> x |. Js.String.make |. fail
      )
    );

    Util.Future.test "fails and returns UnexpectedEmptyArray" (fun _ ->
      Animal.Sql.insert db "snow leopard"
      |. Future.flatMapOk (fun _ -> Animal.Sql.archive_compound_by db "liger")
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun e ->
        (
          match e with
          | Exn.UnexpectedEmptyArray _ -> pass
          | e -> e |. Js.String.make |. fail
        )
        |. Belt.Result.Ok
        |. Future.value
      )
    );

    Util.Future.test "fails and returns EmptyUserQuery" (fun _ ->
      Animal.Sql.insert db "cheetah"
      |. Future.flatMapOk (fun _ ->
          Animal.Sql.archive_compound_by_empty db "cheetah"
      )
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun e ->
        (
          match e with
          | Exn.EmptyUserQuery _ -> pass
          | e -> e |. Js.String.make |. fail
        )
        |. Belt.Result.Ok
        |. Future.value
      )
    )
  );

  describe "Archive.compound_one_by_id" (fun _ ->

    Util.Future.test "returns one proper result" (fun _ ->
      Animal.Sql.insert db "bear"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (Animal.Sql.archive_compound_one_by_id db)
      |. Future.mapOk (fun r ->
        match r with
        | Some (
          {category = "bear"; deleted = 1; deleted_timestamp = None; _ } as x
          ) -> x |. Js.String.make |. fail
        | Some (
          {category = "bear"; deleted = 1; deleted_timestamp = Some(_); _ }
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    );

    Util.Future.test "succeeds but does not return a result" (fun _ ->

      Animal.Sql.insert db "polar bear"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (Animal.Sql.archive_compound_one_by_id_no_result db)
      |. Future.map (fun r ->
        (
          match r with
          | Belt.Result.Ok None -> pass |. Belt.Result.Ok
          | Belt.Result.Ok x -> x |. Js.String.make |. fail |. Belt.Result.Ok
          | Belt.Result.Error e -> Belt.Result.Error e
        )
      )
    );

    Util.Future.test "fails and returns NotFound" (fun _ ->

      Json.Encode.string "999"
      |. CF.Id.fromJson
      |> Animal.Sql.archive_compound_one_by_id db
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun r ->
        (
          match r with
          | Exn.NotFound _ -> pass |. Belt.Result.Ok
          | e -> e |. Belt.Result.Error
        )
        |. Future.value
      )
    )
  );

  describe "Delete.where" (fun _ ->

    Util.Future.test "returns 2 results" (fun _ ->
      Person.Sql.insert db "Arthur" 42
      |. Future.flatMapOk (fun _ -> Person.Sql.insert db "Ford" 42)
      |. Future.flatMapOk (fun _ -> Person.Sql.delete_where db 42)
      |. Future.mapOk (fun r ->
        match r with
        | (
            [|
              { first_name = "Arthur"; age = 42; _ };
              { first_name = "Ford"; age = 42; _ };
            |]
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    );

    Util.Future.test "fails and returns UnexpectedEmptyArray" (fun _ ->
      Person.Sql.delete_where db 999
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun e ->
        (
          match e with
          | Exn.UnexpectedEmptyArray _ -> pass |. Belt.Result.Ok
          | e -> e |. Belt.Result.Error
        )
        |. Future.value
      )
    );

    Util.Future.test "fails and returns EmptyUserQuery" (fun _ ->
      Person.Sql.delete_where_empty db 27
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun e ->
        (
          match e with
          | Exn.EmptyUserQuery _ -> pass |. Belt.Result.Ok
          | e -> e |. Belt.Result.Error
        )
        |. Future.value
      )
    )
  );

  describe "Delete.one_by_id" (fun _ ->

    Util.Future.test "returns one result" (fun _ ->
      Animal.Sql.insert db "dolphin"
      |. Future.mapOk (fun x -> x |. Belt.Option.getExn |. Animal.id)
      |. Future.flatMapOk (Animal.Sql.delete_one_by_id db)
      |. Future.mapOk (fun r ->
        match r with
        | { category = "dolphin"; _ } -> pass
        | x -> x |. Js.String.make |. fail
      )
    );

    Util.Future.test "fails and returns NotFound" (fun _ ->

      Json.Encode.string "999"
      |. CF.Id.fromJson
      |> Animal.Sql.delete_one_by_id db
      |. Future.mapOk (fun x -> x |. Js.String.make |. fail)
      |. Future.flatMapError (fun r ->
        (
          match r with
          | Exn.NotFound _ -> pass |. Belt.Result.Ok
          | e -> e |. Belt.Result.Error
        )
        |. Future.value
      )
    )
  )
);
