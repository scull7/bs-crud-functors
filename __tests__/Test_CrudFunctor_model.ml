open Jest
open CrudFunctor.Sql

let debug = Debug.make "bs-crud-functors" "test:CrudFunctor_model"

module Exn = CrudFunctor_exn
module NFL = Util_nfl
module Team = Util_nfl.Team

let db = Util.Connection.connect ()

let database_name = "TEST_CF_MODEL"

let mutate = Util.Init.mutate debug db

let () =

describe "CrudFunctor_model" (fun _ ->

  let _ = beforeAllPromise (fun _ ->
    Util.Init.run debug db database_name
    |> Js.Promise.then_ (fun _ -> NFL.Team.Sql.init mutate)
  )
  in
  let _ = afterAll (fun _ -> Util.Driver.Connection.close db)
  in

  describe "Read.one_by" (fun _ ->

    Util.Future.test "should retrieve the Steelers" (fun _ ->
      Team.get_by_nickname "steelers" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun (team: Team.team) ->
          match team with
          | ({ city = "pittsburgh"; nickname = "steelers"; active = 1; _ }) ->
              pass
          | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Read.one_by_id" (fun _ ->

    Util.Future.test "should return the inserted team" (fun _ ->
      Team.insert "tennessee" "titans" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id; _ }: Team.team) -> Team.Read.one_by_id id db)
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun (team: Team.team) ->
        match team with
        | ({ city = "tennessee"; nickname = "titans"; active = 1; _ }) ->
            pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Read.raw" (fun _ ->

    Util.Future.test "should return the 49ers" (fun _ ->
      let table = Team.table in
      Team.Read.raw
        (fun base -> base |. Select.where {j|AND $table.city = ?|j})
        Json.Encode.([| string "san francisco" |])
        db
      |. Future.mapOk (fun (r: Team.team array) ->
        match r with
        | [| { city = "san francisco"; nickname = "49ers" } |] -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Read.where" (fun _ ->

    Util.Future.test "should return the Panthers" (fun _ ->
      let table = Team.table in
      Team.Read.where
        (fun base -> base |. Select.where {j|AND $table.city = ?|j})
        Json.Encode.([| string "carolina" |])
        db
      |. Future.mapOk (fun (r: Team.team array) ->
        match r with
        | [| { city = "carolina"; nickname = "panthers" } |] -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Create.batch" (fun _ ->
    Util.Future.test "should return the inserted " (fun _ ->
      Team.Create.batch
        ~name:"Test Create.batch"
        ~encoder:(fun (city, nickname) ->
          Json.Encode.([| (string city); (string nickname); |])
        )
        ~loader:(fun a ->
          Belt.Array.map a (fun (_, nickname) -> nickname)
          |. Team.get_by_nickname_list db
        )
        ~error:(fun str -> Exn.MutationFailure str)
        ~columns:[| "city"; "nickname"; |]
        ~rows: [|
          ("new york", "giants");
          ("new york", "jets");
        |]
        db
      |. Future.mapOk (fun (r: Team.team array) ->
        match r with
        | (
          [|
            { city = "new york"; nickname = "jets"; _ };
            { city = "new york"; nickname = "giants"; _ };
          |]
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Update.by_id" (fun _ ->
    Util.Future.test "should return the update object" (fun _ ->
      Team.insert "oakland" "raiders" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) ->
        let encoder x = Json.Encode.(object_ [ ("city", string x); ])
        in
        Team.Update.by_id encoder "las vegas" id db
      )
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun updated ->
        match updated with
        | { city = "las vegas"; nickname = "raiders"; _ } -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Update.increment_one_by_id" (fun _ ->
    Util.Future.test "should increment the likes column by one" (fun _ ->
      Team.insert "miami" "dolphins" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) ->
        Team.Update.increment_one_by_id "likes" id db
      )
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun updated ->
        match updated with
        | { city = "miami"; nickname = "dolphins"; likes = 1; _ } -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Deactivate.one_by_id" (fun _ ->
    Util.Future.test "should mark the Houton Oilers as inactive" (fun _ ->
      Team.insert "houston" "oilers" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) -> Team.Deactivate.one_by_id id db)
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun deactivated ->
        match deactivated with
        | { city = "houston"; nickname = "oilers"; active = 0; _ } -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Archive.one_by_id" (fun _ ->
    Util.Future.test "should mark the Racine Legion as deleted" (fun _ ->
      Team.insert "racine" "legion" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) -> Team.Archive.one_by_id id db)
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun deactivated ->
        match deactivated with
        | (
            {
              city = "racine";
              nickname = "legion";
              deleted = 1;
              deleted_timestamp = None;
              _
            }
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Archive.compound_by" (fun _ ->
    Util.Future.test "should mark all the new teams as archived" (fun _ ->
      let teams = [|
        ("akron", "pros");
        ("dayton", "triangles");
        ("decatur", "staleys");
      |]
      in
      Team.insert_batch teams db
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "pros" 42 db)
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "triangles" 42 db)
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "staleys" 42 db)
      |. Future.flatMapOk (fun _ -> Team.archive_by_like_count 42 db)
      |. Future.mapOk (fun archived ->
        (
          match archived with
          | [|
              {
                city = "decatur";
                nickname = "staleys";
                deleted = 1;
                deleted_timestamp = Some(_);
                _
              };
              {
                city = "dayton";
                nickname = "triangles";
                deleted = 1;
                deleted_timestamp = Some(_);
                _
              };
              {
                city = "akron";
                nickname = "pros";
                deleted = 1;
                deleted_timestamp = Some(_);
                _
              };
            |] -> pass
          | x -> x |. Js.String.make |. fail
        )
      )
    )
  );

  describe "Archive.compound_one_by_id" (fun _ ->
    Util.Future.test "should mark the Toledo Maroons as deleted" (fun _ ->
      Team.insert "kenosha" "maroons" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) -> Team.Archive.compound_one_by_id id db)
      |. Future.mapOk Belt.Option.getExn
      |. Future.mapOk (fun deactivated ->
        match deactivated with
        | (
            {
              city = "kenosha";
              nickname = "maroons";
              deleted = 1;
              deleted_timestamp = Some(_);
              _
            }
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Delete.one_by_id" (fun _ ->
    Util.Future.test "should remove the team from the database" (fun _ ->
      Team.insert "rock island" "independents" db
      |. Future.mapOk Belt.Option.getExn
      |. Future.flatMapOk (fun ({ id }) -> Team.Delete.one_by_id id db)
      |. Future.mapOk (fun deleted ->
        match deleted with
        | (
            {
              city = "rock island";
              nickname = "independents";
              deleted = 0;
              deleted_timestamp = None;
              _
            }
          ) -> pass
        | x -> x |. Js.String.make |. fail
      )
    )
  );

  describe "Delete.where" (fun _ ->
    Util.Future.test "should delete all the new teams" (fun _ ->
      let teams = [|
        ("chicago", "tigers");
        ("chicago", "cardinals");
        ("rochester", "jeffersons");
      |]
      in
      Team.insert_batch teams db
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "tigers" 13 db)
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "cardinals" 13 db)
      |. Future.flatMapOk (fun _ -> Team.set_likes_by_nickname "jeffersons" 13 db)
      |. Future.flatMapOk (fun _ -> Team.delete_by_like_count 13 db)
      |. Future.mapOk (fun deleted ->
        (
          match deleted with
          | [|
              {
                city = "rochester";
                nickname = "jeffersons";
                deleted = 0;
                deleted_timestamp = None;
                _
              };
              {
                city = "chicago";
                nickname = "cardinals";
                deleted = 0;
                deleted_timestamp = None;
                _
              };
              {
                city = "chicago";
                nickname = "tigers";
                deleted = 0;
                deleted_timestamp = None;
                _
              };
            |] -> pass
          | x -> x |. Js.String.make |. fail
        )
      )
    )
  );

)
