[![NPM](https://nodei.co/npm/bs-crud-functors.png)](https://nodei.co/npm/bs-crud-functors/)
[![pipeline status](https://gitlab.com/scull7/bs-crud-functors/badges/master/pipeline.svg)](https://gitlab.com/scull7/bs-crud-functors/commits/master)
[![coverage report](https://gitlab.com/scull7/bs-crud-functors/badges/master/coverage.svg)](https://scull7.gitlab.io/bs-crud-functors/)

# bs-crud-functors
ReasonML model and query generator for CRUD actions based upon
[bs-sql-common] and [bs-sql-composer].

## Why?

This is a SQL wrapper that provides many convenience features and a "light" ORM interface that
uses plain SQL as the underlying language.  Currently it is only compatible with the [bs-mysql2]
client.

## How do I install it?

Inside of a BuckleScript project:
```shell
yarn add @glennsl/bs-json bs-crud-functors bs-mysql2 bs-node-debug bs-sql-common bs-sql-composer
```

Then add `@glennsl/bs-json`, `bs-crud-functors`, `bs-mysql2`, `bs-node-debug`,
`bs-sql-common` and `bs-sql-composer` to your `bs-dependencies` in
`bsconfig.json`:

```json
{
  "bs-dependencies": [
    "@glennsl/bs-json",
    "bs-crud-functors",
    "bs-mysql2",
    "bs-node-debug",
    "bs-sql-common",
    "bs-sql-composer"
  ]
}
```

## How do I use it?

### General Usage

The way you should access modules in `CrudFunctors` is as follows:

```reason
CrudFunctors.<Module>
```

Of course you can always directly call the internal files, namespaced with
`CrudFunctors_`, but that is not recommended since these files are
implementation details.

### Using the Factory Model.

Below are the requirements necessary to use the FactoryModel. Each requirement
is documented with examples below.  The requirements include: creating the
connection, creating the config, and creating the model.

#### Creating the Connection

```reason
module Sql = SqlCommon.Make_sql(MySql2);
module CrudFunctorImpl = CrudFunctor.Make(Sql)

let conn = Sql.connect(
  ~host="127.0.0.1",
  ~port=3306,
  ~user="root",
  ~database="example",
  ()
);
```


#### Creating the Config

Creating the Config is quite simple, below is a brief explanation
for each field in the Config:

- t: the record type that will be mapped to
- table: the name of the database table
- decoder: a function to map the query response to t
- base: the base query for the model, try to keep this as thin as possible
  (i.e. minimal where clauses, etc.)

```reason
let table = "animal";

type animal = {
  id: int,
  type_: string,
  deleted: int,
};

module Config = {
  type t = animal;
  let table = table;
  let decoder = json =>
    Json.Decode.{
      id: field("id", int, json),
      type_: field("type_", string, json),
      deleted: field("deleted", int, json),
    };
  let base =
    SqlComposer.Select.(
      select
      |> field({j|$table.`id`|j})
      |> field({j|$table.`type_`|j})
      |> field({j|$table.`deleted`|j})
      |> order_by(`Desc({j|$table.`id`|j}))
    );
};
```

#### Creating the Model

Creating the model is quite simple once the Config is setup:

```reason
module Model = CrudFunctorImpl.Model.Make(Config);
```

#### Usage Examples

Below are a few examples on how to use the model, refer to the documentation
below for the full list of functions/methods:

```reason
Model.Create.one(
  type_ => Json.Encode.([ ("type_", string @@ type_) ] |. object_),
  "my cat",
  conn
)
|. Future.mapOk(maybeCat =>
  switch(maybeCat) {
  | Some(cat) => Js.log2("Your New Cat: ", cat)
  | None => Js.log("Oh the noes, we lost your cat")
  }
)
|. Future.flatMapError(error => Js.Console.error(error))
|. ignore;

Model.Read.where(
  (base) => base |. SqlComposer.Select.where({j|AND $table.type_ = ?|j}),
  Json.Encode.([| string @@ "cat" |]),
  conn
)
|. Future.mapOk(cats => Js.log2("Your Cats:", cats))
|. Future.flatMapError(error => Js.Console.error(error))
|. ignore;
```

Creating an ID can be a pain-point, but it's abstracted away to ensure proper
handling and formatting.

```reason
let id  = 42 |. Json.Encode.int |. string_of_int |. CrudFunctorImpl.Id.fromJson;
Model.Read.one_by_id(id, db)
|. Future.mapOk(maybeCat =>
  switch(maybeCat) {
  | Some(cat) => Js.log2("Your Cat: ", cat)
  | None => Js.log("Oh the noes, we couldn't find your cat")
  }
)
|. Future.flatMapError(error => Js.Console.error(error))
|. ignore;
```

*Note: you will notice that some methods will return `Result.Ok(None)`, this means that the row(s)
were altered successfully but when an attempt to fetch the same row(s) was made the operation failed;
this is because the Model's base query filters out the row(s) after update.*

## What's missing?

Everything not checked...

- [ ] Query Interface
  - [x] _(raw)_ Raw SQL query
  - [ ] _(rawInsert)_ Raw SQL insert
  - [ ] _(rawUpdate)_ Raw SQL update
  - [x] INSERT
    - [x] _(insertOne)_ basic wrapper
    - [x] _(insertBatch)_ basic wrapper
  - [ ] UPDATE
    - [x] _(updateOneById)_ Basic wrapper using the `id` column - must fit `PrimaryId` interface
    - [ ] _(updateWhereParams)_ with the `ObjectHash` interface
    - [x] _(incrementOneById)_ increment an integer column using the `id` column - must fit the `Counter`
          and `PrimaryId` interfaces
  - [x] DELETE
    - [x] _(deleteBy)_ using a custom where clause
    - [x] _(deleteOneById)_ - must fit the `PrimaryId` interface
  - [x] Archive
    - [x] _(deactivateOneById)_ Deactivate a row using the `id` column - must fit the `Activated` and
          `PrimaryId` interfaces
    - [x] _(archiveOneById)_ Soft DELETE a row using the `id` column - must fit the `Archive` interface
    - [x] _(archiveCompoundBy)_ Soft Compound DELETE using a custom where clause - must fit the
          `ArchiveCompound` interface
    - [x] _(archiveCompoundOneById)_ Soft Compound DELETE a row using the `id` column - must fit the
          `ArchiveCompound` and `PrimaryId` interfaces
  - [ ] SELECT
    - [ ] Transforms
      - [ ] JSON column parse
      - [ ] Nest dot notation transform
      - [ ] Null out nested objects
    - [x] _(get)_ using the Compositional SQL DSL
    - [x] _(getByIdList)_ using the `id` column - must fit `PrimaryId` interface
    - [x] _(getOneBy)_ with custom where clause
    - [x] _(getOneById)_ using the `id` column - must fit `PrimaryId` interface
    - [x] _(getWhere)_ using a custom where clause
    - [ ] _(getWhereParams)_ using the `ObjectHash` interface
- [ ] Model
  - [x] Compositional SQL DSL
  - [x] Model Creation DSL
  - [x] Query interface
    - [ ] INSERT
      - [x] _(insertOne)_
      - [x] _(insertBatch)_
      - [ ] Pre-Create intercept
      - [ ] Post-Create intercept
    - [ ] UPDATE
      - [x] _(updateOneById)_ using the `id` column - must fit `PrimaryId` interface
      - [x] _(incrementOneById)_ increment an integer column using the `id` column - must fit the `Counter`
            and `PrimaryId` interfaces
      - [ ] Pre-Update intercept
      - [ ] Post-Update intercept
    - [x] DELETE
      - [x] _(deleteBy)_ using a custom where clause
      - [x] _(deleteOneById)_ - must fit the `PrimaryId` interface
    - [x] Archive
      - [x] _(deactivateOneById)_ Deactivate a row using the `id` column - must fit the `Activated` and
            `PrimaryId` interfaces
      - [x] _(archiveOneById)_ Soft DELETE a row using the `id` column - must fit the `Archive` interface
      - [x] _(archiveCompoundBy)_ Soft Compound DELETE using a custom where clause - must fit the
            `ArchiveCompound` interface
      - [x] _(archiveCompoundOneById)_ Soft Compound DELETE a row using the `id` column - must fit the
            `ArchiveCompound` and `PrimaryId` interfaces
    - [ ] SELECT
      - [ ] Transforms - _(Dependent upon Query Interface implementation)_
      - [x] _(get)_ using the Compositional SQL DSL
      - [x] _(getByIdList)_ using the `id` column - must fit `PrimaryId` interface
      - [x] _(getOneBy)_ with custom where clause
      - [x] _(getOneById)_ using the `id` column - must fit `PrimaryId` interface
      - [x] _(getWhere)_ using a custom where clause
      - [ ] _(getWhereParams)_ using the `ObjectHash` interface
- [ ] Search - _This needs some re-design to better fit ReasonML language semantics._
- [ ] Utilities
  - [ ] TIMESTAMP conversion
  - [ ] `ObjectHash` interface interpolation
  - [ ] Caching

[bs-mysql]: https://github.com/davidgomes/bs-mysql
[bs-sql-common]: https://gitlab.com/scull7/bs-sql-common
[bs-sql-composer]: https://gitlab.com/scull7/bs-sql-composer
[mysql2]: https://www.npmjs.com/package/mysql2
[pimp-my-sql]: https://github.com/influentialpublishers/pimp-my-sql
